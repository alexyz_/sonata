<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Description of CustomViewAdmin
 *
 * @author h3eal
 */
class CustomViewAdmin extends AbstractAdmin
{

	protected $baseRoutePattern = 'custom_view';
	protected $baseRouteName = 'custom_view';

	protected function configureRoutes(RouteCollection $collection)
	{
		$collection->clearExcept(['list', 'import']);
	}

	public function configureActionButtons($action, $object = null)
	{
		$list = parent::configureActionButtons($action, $object);

		$list['import']['template'] = 'import_button.html.twig';

		return $list;
	}

	public function getDashboardActions()
	{
		$actions = parent::getDashboardActions();

		$actions['import'] = [
			'label' => 'import_action',
			'translation_domain' => 'SonataAdminBundle',
			'url' => '',
			'icon' => 'level-up',
		];

		return $actions;
	}

}
