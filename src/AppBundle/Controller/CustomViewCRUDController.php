<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Controller;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of CustomViewCRUDController
 *
 * @author h3eal
 */
class CustomViewCRUDController extends CRUDController
{
	public function importAction(Request $request)
    {
        //do your import logic
    }
	
    public function listAction()
    {
		$var = "hello world";
        return $this->renderWithExtraParams('admin/custom_view.html.twig', ['var' => $var]);
    }
}
